import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from sklearn.metrics import accuracy_score
import numpy as np
import csv

class IA:
	def __init__(self, data = pd.read_csv('dataset.csv'), newData = pd.DataFrame({
			'expected_hours': [24],
			'total_sp': [1]
		})
	):
		self.data = data
		self.newData = newData

	def setData(self, data):
		self.data = data

	def getData(self):
		return self.data
	
	def setNewData(self, newData):
		self.newData = newData

	def getNewData(self):
		return self.newData

	def madePredict(self, data = pd.read_csv('dataset.csv'), newData = pd.DataFrame({
		'expected_hours': [33.6],
		'total_sp': [10]
	})):
		self.data = data
		self.newData = newData

		x = data[['total_sp', 'expected_hours']]
		y = data['finished']

		SEED = 20
		np.random.seed(SEED)
		raw_treino_x, raw_teste_x, treino_y, teste_y = train_test_split(x, y, test_size = 0.25, stratify = y)
		print("Treinaremos com %d elementos e testaremos com %d elementos" % (len(raw_treino_x), len(raw_teste_x)))

		modelo = DecisionTreeClassifier(max_depth=2)
		modelo.fit(raw_treino_x, treino_y)

		new_train_x = newData[['total_sp', 'expected_hours']]
		previsoes = modelo.predict(new_train_x)
		print(previsoes[0])
		newData['finished'] = previsoes[0]

		newData.to_csv('dataset.csv', mode='a', header=False, index=False)

		# acuracia = accuracy_score(teste_y, previsoes) * 100
		# print("A acurácia foi %.2f%%" % acuracia)

		# import graphviz
		# features = x.columns
		# dot_data = export_graphviz(modelo, filled=True, rounded=True,feature_names=features, class_names=["não", "sim"])
		# grafico = graphviz.Source(dot_data)
		# grafico

anthem = IA()
anthem.madePredict(anthem.getData(), anthem.getNewData())
